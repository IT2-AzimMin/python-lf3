def subnet_calc():
    # Take input from the user for the base network and number of subnets
    base_network = input("Enter the base network (in CIDR notation, e.g. 192.168.0.0/24): ")
    num_subnets = int(input("Enter the number of subnets to create: "))

    # Convert the base network to an IP address object
    base_network_ip = base_network.split('/')[0]
    base_network_mask = int(base_network.split('/')[1])
    base_network_ip_obj = [int(x) for x in base_network_ip.split('.')]
    base_network_ip_obj = (base_network_ip_obj[0]<<24)+(base_network_ip_obj[1]<<16)+(base_network_ip_obj[2]<<8)+base_network_ip_obj[3]

    # Calculate the number of bits required for the network ID
    bits = 0
    while (1<<bits) < num_subnets:
        bits += 1

    # Check if there are too many subnets for the given base network
    if bits > (32 - base_network_mask):
        print("Error: Too many subnets for the given base network")
        return

    # Calculate the size of each subnet
    subnet_size = 1<<(bits)
    num_addresses = subnet_size - 2

    # Output the subnets
    for i in range(num_subnets):
        network_ip = base_network_ip_obj + i*subnet_size
        network_address = f"{network_ip>>24}.{(network_ip>>16)&0xFF}.{(network_ip>>8)&0xFF}.{network_ip&0xFF}"
        subnet_mask = f"{32 - base_network_mask - bits}"
        print(f"Network {i+1}: {network_address}/{subnet_mask} (Max hosts: {num_addresses})")


# Call the subnet calculator function
subnet_calc()

